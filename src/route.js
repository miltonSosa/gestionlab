import { Router } from 'express'
import { pageRoot, apiRoot } from './config'
import httpErrors from 'http-errors'
import Page from './page'
import Api from './api'

const router = new Router()

router.use(pageRoot, Page)
router.use(apiRoot, Api)


router.use((err, req, res, next) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
  
    // render the error page
    res.status(err.status || 500);
    res.render('error');
});


// catch 404 and forward to error handler
// router.use((req, res, next) => {
//   next(httpErrors(404));
// });

export default router

// import { Router } from 'express'


// const router = new Router()

// router.use(pageRoot, Page)

// // error handler
// // router.use((err, req, res, next) => {
// //     // set locals, only providing error in development
// //     res.locals.message = err.message;
// //     res.locals.error = req.app.get('env') === 'development' ? err : {};
  
// //     // render the error page
// //     res.status(err.status || 500);
// //     res.render('error');
// //   });


// // // catch 404 and forward to error handler
// // router.use((req, res, next) => {
// //   next(httpErrors(404));
// // });


// export default router