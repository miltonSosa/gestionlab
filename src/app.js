import http from 'http'
import { env, mongo, port, ip, apiRoot, pageRoot } from './config'
import mongoose from './services/mongoose'
import express from './services/express'
import Route from './route'

const app = express('', Route)
const server = http.createServer(app)

if (mongo.uri) {
  mongoose.connect(mongo.uri)
}
mongoose.Promise = Promise

setImmediate(() => {
  server.listen(port, ip, () => {
    console.log('Express server listening on http://%s:%d, in %s mode', ip, port, env)
  })
})

export default app
