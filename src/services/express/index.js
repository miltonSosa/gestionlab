import express from 'express'
import forceSSL from 'express-force-ssl'
import cors from 'cors'
import compression from 'compression'
import morgan from 'morgan'
import path from 'path'
import bodyParser from 'body-parser'
import { errorHandler as queryErrorHandler } from 'querymen'
import { errorHandler as bodyErrorHandler } from 'bodymen'
import { env } from '../../config'
import nunjucks from 'nunjucks'


export default (apiRoot, routes) => {

  const app = express()

  /* istanbul ignore next */
  if (env === 'production') {
    app.set('forceSSLOptions', {
      enable301Redirects: false,
      trustXFPHeader: true
    })
    app.use(forceSSL)
  }

  /* istanbul ignore next */
  if (env === 'production' || env === 'development') {
    app.use(cors())
    app.use(compression())
    app.use(morgan('dev'))
  }

  app.set('views', path.join(__dirname,'./../../views'));
  // view engine setup
  app.set('view engine', 'njk');
  nunjucks.configure(path.join(__dirname,'./../../views'), {
    autoescape: true,
    express: app,
    web:{
      async: true
    }
  });

  nunjucks.Loader.extend({
    async: true,
    getSource: function(name, callback) {
        console.log(name)
        callback(err, res);
    }
  });


  app.use(express.static(path.join(__dirname,'./../../public')));


  app.use(bodyParser.urlencoded({ extended: false }))
  app.use(bodyParser.json())
  app.use(apiRoot, routes)
  app.use(queryErrorHandler())
  app.use(bodyErrorHandler())

  return app
}
