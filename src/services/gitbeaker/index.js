import { Gitlab } from '@gitbeaker/node'; // All Resources
import { gitlabHost, gitlabToken } from '../../config'

// import { Projects, Groups, GroupSchema, ProyectSchema } from '@gitbeaker/node'; // Just the Project Resource
// import { Types } from '@gitbeaker/node';

const gitlab = new Gitlab({
    token: gitlabToken,
    host: gitlabHost
});

// let projects = gitlab.Projects.all({ maxPages: 2, perPage: 40 }).then(()=>{
//     console.log('entro a buscar proyectos')
// });

let groups = gitlab.Groups.all().then((resp)=>{
    return resp
});

// console.log('imprimo proyectos');
// console.log(projects);

export { gitlab as default, groups }
