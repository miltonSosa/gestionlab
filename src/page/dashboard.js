import { response, Router } from 'express'
import gitlab from "../services/gitbeaker";

const router = new Router()

/* GET index page. */
router.get('/', (req, res) => {
  console.log('Entro al Dash Board');
  const groups = gitlab.Groups.all()
      .then((data)=>{
        console.log(data);
        return groups
  });
  res.render('index', {
    groups,
    title: 'Express'
  });
})
      

export default router