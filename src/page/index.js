import { Router } from 'express'
import dashboard from './dashboard'

const router = new Router()

router.use('/', dashboard)

export default router